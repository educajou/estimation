const milieu=document.getElementById('milieu');
const droite=document.getElementById('droite');
const debut=document.getElementById('debut');
const fin=document.getElementById('fin');
const mobile=document.getElementById('mobile');
const etiquetteMobile=document.getElementById('etiquette-mobile');
const erreur=document.getElementById('erreur');
const premiereGraduation=document.getElementById('premiere-graduation');
const nombre=document.getElementById('nombre');
const suivant=document.getElementById('suivant');
const suivantbis=document.getElementById('suivantbis');
const ok=document.getElementById('ok');
const boutonGraduations=document.getElementById('boutton-graduations');
const menu=document.getElementById('menu');
const stats=document.getElementById('stats');
const boutonMenu=document.getElementById('bouton-menu');
const boutonStats=document.getElementById('bouton-stats');
const resultats=document.getElementById('resultats');
const choixA=document.getElementById('choix-a');
const choixB=document.getElementById('choix-b');
const choixVerrA=document.getElementById('choix-verr-a');
const choixVerrB=document.getElementById('choix-verr-b');
const a1=document.getElementById('a1');
const a2=document.getElementById('a2');
const b1=document.getElementById('b1');
const b2=document.getElementById('b2');
const boutonEffacer=document.getElementById('bouton-effacer-stats');
const boutonReponse1=document.getElementById('bouton-reponse-1');
const boutonReponse2=document.getElementById('bouton-reponse-2');
const boutonReponse3=document.getElementById('bouton-reponse-3');
const boutonReponse4=document.getElementById('bouton-reponse-4');
const boutonReponse5=document.getElementById('bouton-reponse-5');
const boutonReponse6=document.getElementById('bouton-reponse-6');
const darkbox = document.getElementById('darkbox');
const divApropos = document.getElementById('apropos');
const body=document.body;
const blocUnite=document.getElementById('bloc-unite');
const spanTauxReussite=document.getElementById('taux-reussite');
const divReussite=document.getElementById('reussite');



//Paramètres URL

let numeroExercice;
let modeSaisie;

let url = window.location.search;
let urlParams = new URLSearchParams(url);

if (urlParams.get('primtuxmenu')==="true") {
    boutonMenu.style.top='90px';
    menu.style.top='80px';
    body.style.backgroundImage='url(images/primtux.png)';
}

if (urlParams.get('min')) {
    a=parseInt(urlParams.get('min'));
} else {
    a=0;
}

if (urlParams.get('max')) {
    let nombre=parseInt(urlParams.get('max'));
    if (nombre>a+5){b=nombre;}
    else {b=a+10}    
} else {
    b=a+10;
}

if (urlParams.get('mode')) {
    modeSaisie = urlParams.get('mode');
} else {
    modeSaisie = 'qcm';
}

changeMode(modeSaisie);

if (urlParams.get('mathalea') !== null) {

    console.log('Math Alea');


    numeroExercice = urlParams.get('numeroExercice');  

    modeFonctionnement = urlParams.get('vue');

    console.log('Numéro exercice '+numeroExercice);
    console.log('Mode fonctionnement '+modeFonctionnement);

} else {
    modeFonctionnement = 'normal';
}


// Variables globales
dragged=null;
juste=false;
aidePremiereGraduation=false;
aideGraduations=false;
aideBlocUnite=false;
lock=false;
nbMobile=null;
menuOn=false;
petitEcran=false;
zoneSaisie=nombre;
let tauxReussite;
let reussites = [0,0];
let reponse1;
let reponse2;
let reponse3;
let reponse4;
let reponse5;
let reponse6;
let reponses=[reponse1,reponse2,reponse3,reponse4,reponse5,reponse6];
let boutonsReponses=[boutonReponse1,boutonReponse2,boutonReponse3,boutonReponse4,boutonReponse5,boutonReponse6];

// Nombres min / max

lockA=true;
lockB=true;

// Réglages des boutons;
choixA.value=a;
choixB.value=b;
a2.checked=true;
b2.checked=true;

// Autres réglages
nombre.style.fontSize='64px';

if (modeFonctionnement === 'prof') {
    let contenuRecupere = '';
    resultats.innerHTML=contenuRecupere;
    visibiliteStats('ouvre');
}

if (modeFonctionnement === 'eleve') {
    boutonEffacer.innerHTML='Envoyer';
}



// Début des fonctions


function changeMode(mode) {

    console.log('mode actuel : '+modeSaisie)
    console.log('changement de mode => '+mode)
    modeSaisie = mode;
    console.log('nouveau mode : '+modeSaisie);

    let elementsQcm = document.querySelectorAll('.modeqcm');
    let elementsClavier = document.querySelectorAll('.modeclavier');


    // Masquer les éléments du mode à quitter

    if (modeSaisie==='qcm'){
        document.getElementById('boutonqcm').checked = true;
        body.classList.add('qcm');
        elementsClavier.forEach(element => {
            element.classList.add('hide');
    });

    } else {
        document.getElementById('boutonclavier').checked = true;
        body.classList.remove('qcm');
        elementsQcm.forEach(element => {
            element.classList.add('hide');
        });
    }

    // Afficher les éléments du mode à adopter

    if (modeSaisie==='clavier'){
        elementsClavier.forEach(element => {
            element.classList.remove('hide');
    });

    } else {
        elementsQcm.forEach(element => {
            element.classList.remove('hide');
        });
    }

    const urlCourante = new URL(window.location);
    urlCourante.searchParams.set('mode', modeSaisie);
    window.history.replaceState({}, '', urlCourante);  // Mettre à jour l'URL sans recharger la page
    

}


function maj(entree){    
    if (entree===choixA){
        a=parseInt(entree.value);
        if (a==b-1){
            b=b+1;
            choixB.value=b;
        }
        verifieValeurA();
    } else if (entree===choixB){
        b=parseInt(entree.value);
        if (b==a+1){            
            a=a-1;
            choixA.value=a;
        }
        verifieValeurB();
    }
    
    // Mise à jour des paramètres d'URL
    const urlCourante = new URL(window.location);
    urlCourante.searchParams.set('min', a);
    urlCourante.searchParams.set('max', b);
    window.history.replaceState({}, '', urlCourante);  // Mettre à jour l'URL sans recharger la page

    exerciceSuivant();

}

function changelockA(){
    lockA=!lockA;
    exerciceSuivant();

}

function changelockB(){
    lockB=!lockB;
    exerciceSuivant();

}

function active(bouton){
    bouton.classList.toggle('actif');
    bouton.blur();
}

function tirerBornesAuSort(a,b){
    // Vérification que a < b
    if (a >= b) {
        throw new Error("a doit être inférieur à b");
    }

    // Génération de deux nombres aléatoires entre a et b
    let nbDebut = Math.floor(Math.random() * (b-a-2)) + a ;
    let nbFin = Math.floor(Math.random() * (b-nbDebut-1)) + nbDebut +2 ;
    return [nbDebut, nbFin];
}

function tirerNombreauSort(min, max) {
    console.log("tirage au sort entre "+min+" et "+max+" (exclus)");
    return Math.floor(Math.random() * (max - min - 1)) + min + 1;    
}


// Fonction utilitaire pour mélanger un tableau
function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
}


// Fonction utilitaire pour vérifier si un nombre est dans une plage donnée
function estTropProche(valeur, reference, nbDebut, nbFin, pourcentage) {
    const plage = nbFin - nbDebut;  // Calcul de la plage entre nbDebut et nbFin
    const epsilon = Math.abs(plage * pourcentage / 100);  // Calcul de l'epsilon basé sur cette plage
    return valeur >= reference - epsilon && valeur <= reference + epsilon;
}

function changeQcm() {

    console.log('change qcm');
    console.log(nbDebut);
    console.log(nbFin);

    const plage = nbFin - nbDebut; 

    reponses = [];
    reponse1 = nbMobile;
    let mini = plage < 3;

    if (mini) {

        if (nbDebut === 0) {

            reponses = [
                nbMobile,
                nbDebut,
                nbFin,
                nbFin+1,
                nbFin+2,
                nbFin+3
            ]

        } else if (nbDebut === 1) {

            reponses = [
                nbMobile,
                nbDebut,
                nbFin,
                nbFin+1,
                nbFin+2,
                0
            ]
            
        }

        else if (nbDebut === 2) {

            reponses = [
                nbMobile,
                nbDebut,
                nbFin,
                nbFin+1,
                0,
                1
            ]
            
        }

        else {

            reponses = [
                nbMobile,
                nbDebut,
                nbFin,
                nbFin+1,
                nbDebut-1,
                nbFin+2
            ]
            
        }
    } else {

        console.log("Bonne réponse : "+reponse1);
        reponses.push(reponse1);

        // Générer les réponses pour reponse2 à reponse4
        let reponsesDisponibles = [];
        for (let i = nbDebut+1; i <= nbFin-1; i++) {
            if (!estTropProche(i, reponse1, nbDebut, nbFin, 20)) {
                reponsesDisponibles.push(i);
            }
        }
        

            // S'assurer qu'il y a assez de réponses disponibles
        if (reponsesDisponibles.length < 3) {
            console.error('Pas assez de réponses disponibles qui respectent la contrainte de proximité.');
            let nombreDeReponsesACreer = 3 - reponsesDisponibles.length;
            let nombreDeReponsesDisponibles = reponsesDisponibles.length;
            console.log(nombreDeReponsesDisponibles+" réponses disponibles")
            console.log(reponsesDisponibles)
            console.log(nombreDeReponsesACreer+" réponses de substitution à créer");

            for (let i = 0; i < nombreDeReponsesDisponibles; i++) {
                let nouvelleReponse = reponsesDisponibles.pop();
                nombre
                reponses.push(nouvelleReponse);
                console.log("ajout de "+nouvelleReponse);
            }
            

            for (let i = 0; i < nombreDeReponsesACreer; i++) {

                let nouvelleReponse = undefined;

                while (reponses.includes(nouvelleReponse) || nouvelleReponse === undefined) {

                    if (Math.random()>0.5){
                        nouvelleReponse = tirerNombreauSort(nbFin, (nbFin+plage*1.5));
                        } else {
                            if (nbDebut>0){
                                if (nbDebut <= plage){
                                    nouvelleReponse = tirerNombreauSort(-1, nbDebut+1);
                                } else {
                                    if (plage > 3){
                                        nouvelleReponse = tirerNombreauSort(nbDebut - plage, nbDebut+1);
                                    } else {
                                        nouvelleReponse = tirerNombreauSort(nbDebut - (plage*2), nbDebut+1);
                                    }
                                }
                            } else {
                                nouvelleReponse = tirerNombreauSort(nbFin, nbFin+plage*1.5);
                            }
                        }
                    }
                console.log("réponse de substitution : "+nouvelleReponse);
                reponses.push(nouvelleReponse);
                nouvelleReponse = undefined;
            }
        } else {
            // On mélange les réponses disponibles
            reponsesDisponibles = shuffleArray(reponsesDisponibles);
            // On prend trois réponses et on les ajoute à la liste
            for (let i = 0; i < 3; i++) {
                reponses.push(reponsesDisponibles.pop())
            }
        }


        console.log(`reponses : ${reponses}`);


        // Générer les réponses pour reponse5 et reponse6     
        console.log("plage="+plage)
        console.log(nbFin+plage*1.5);
        while (reponses.includes(reponse5) || reponse5 === undefined) {
            if (plage > 3){
            reponse5 = tirerNombreauSort(nbFin, (nbFin+plage*1.5));
            } else {
                reponse5 = tirerNombreauSort(nbFin, (nbFin+plage*3));
            }
        }
        reponses.push(reponse5);
        reponse6 = undefined;
        while (reponses.includes(reponse6) || reponse6 === undefined) {
            console.log('tirage de nombre plus petit');
            if (nbDebut!=0){
                if (nbDebut <= plage){
                reponse6 = tirerNombreauSort(-1, nbDebut+1);
                } else {
                    if (plage > 3){
                        reponse6 = tirerNombreauSort(nbDebut - plage, nbDebut+1);
                    } else {
                        reponse6 = tirerNombreauSort(nbDebut - (plage*2), nbDebut+1);
                    }
                }
            } else {
                reponse6 = tirerNombreauSort(nbFin, nbFin+plage*1.5);
            }
        }

        reponses.push(reponse6);
        

        console.log(`reponses : ${reponses}`);
        console.log(`mélange`);

    }

    // Mélanger les réponses (désactivé pour le débogage)
    reponses = shuffleArray(reponses);
    console.log(`reponses : ${reponses}`);



    // Mettre à jour les boutons avec les réponses
    boutonsReponses.forEach((bouton, index) => {
        console.log(`Bouton ${index + 1} : ${reponses[index]}`);
        bouton.innerHTML = (reponses[index] !== undefined && reponses[index] !== null) ? reponses[index] : '?';
    });
}


function majEtiquettes(){
    debut.innerHTML=nbDebut;
    fin.innerHTML=nbFin;
}

function afficheNombreMobile(){    
    nbMobile=tirerNombreauSort(nbDebut,nbFin);
    let ecartGlobal = nbFin - nbDebut;
    let ecartNbMobile = nbMobile - nbDebut;
    let positionXmobile = (ecartNbMobile / ecartGlobal) * 100;
    mobile.style.display = 'flex';
    mobile.style.left = etiquetteMobile.style.left = positionXmobile + '%';
    changeQcm();
}



// Fonction pour mélanger les éléments d'un tableau
function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        // Générer un index aléatoire entre 0 et i
        const j = Math.floor(Math.random() * (i + 1));
        // Échanger les éléments array[i] et array[j]
        [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
}



function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
      // Générer un index aléatoire entre 0 et i
      const j = Math.floor(Math.random() * (i + 1));
      // Échanger les éléments array[i] et array[j]
      [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
  }

function entreNombre(entree){
    if (!lock){
        if (entree==='⌫'){
            if (zoneSaisie.innerText.length===1){
                zoneSaisie.innerText='?';
                zoneSaisie.style.color='gray';
                nombre.style.fontSize='64px';
            } else {
                zoneSaisie.innerText = zoneSaisie.innerText.substring(0, zoneSaisie.innerText.length - 1);
            }
        } else {
            if (zoneSaisie.innerText==='?'){zoneSaisie.innerText='';zoneSaisie.style.color=null;nombre.style.fontSize=null;}
            zoneSaisie.innerText+=entree;
        }
    }
}

function verifieReponse(index){
    
    console.log("verification de la réponse");
    console.log("Réponse attendue "+nbMobile);
    console.log("index "+index);

    if (juste){exerciceSuivant();}

    else if (index === undefined && zoneSaisie.innerText==='?'){ // pas de réponse
        console.log("pas deréponse");

        zoneSaisie.style.backgroundColor='yellow';
        lock=true;
        setTimeout(function() {
            zoneSaisie.style.backgroundColor = null;
            lock=false;
        }, 1000);
    }
        else {
            console.log("verification");
            reussites[1] +=1;
            let reponse;
            if (index != undefined){
                console.log()
                reponse=parseInt(boutonsReponses[index].innerHTML);
            } else {
                reponse = parseInt(zoneSaisie.innerText);
            }
            console.log("réponse donnée "+reponse);
            if (reponse===nbMobile){  // c'est juste
                if (index === undefined){
                    zoneSaisie.style.backgroundColor='rgb(0,255,0)';
                    suivant.classList.remove('hide');
                    ok.classList.add('hide');
                    erreur.classList.add('hide');
                } else {
                    suivantbis.classList.remove('transparent');
                    boutonsReponses[index].style.backgroundColor='rgb(0,255,0)';                       

                }
                juste=true;
                reussites[0] +=1;
                etiquetteMobile.innerHTML=reponse;
                etiquetteMobile.style.backgroundColor='rgb(0,255,0)';
                
            } else { // c'est faux
                if (!index){
                    zoneSaisie.style.backgroundColor='rgb(255,0,0)';
                }
                lock=true;
                afficheFaux(reponse);
                if (index != undefined){
                    boutonsReponses[index].style.backgroundColor='red';
                }                        
                setTimeout(function() {
                    nombre.style.fontSize='64px';
                    zoneSaisie.innerText='?';
                    zoneSaisie.style.color='gray';
                    zoneSaisie.style.backgroundColor = null;
                    if (index != undefined){
                        boutonsReponses[index].style.backgroundColor=null;
                    }
                    juste=false;
                    lock=false;
                }, 1000);
            }
            resultat(reponse);
        }
}

function afficheFaux(reponse){
    if (nbDebut<=reponse && reponse<=nbFin){
        erreur.classList.remove('hide');
        erreur.style.left= ecartement/facteur * (reponse - nbDebut) + '%';
        erreur.innerText = reponse;        
    }
}

function resultat(reponse){
    let ligne=document.createElement('p');
    resultats.appendChild(ligne);
    tauxReussite = Math.round(100 * reussites[0] / reussites [1]);
    spanTauxReussite.innerHTML = tauxReussite + " %";

    // Calcul de la couleur
    let r, g, b = 0;  // Pas de composante bleue

    if (tauxReussite < 50) {
        // De 0% à 50% : transition du rouge (255, 0, 0) vers le jaune (255, 255, 0)
        r = 255;
        g = Math.round(255 * (tauxReussite / 50));  // Le vert augmente avec le taux de réussite
    } else {
        // De 50% à 100% : transition du jaune (255, 255, 0) vers le vert (0, 255, 0)
        r = Math.round(255 * (1 - (tauxReussite - 50) / 50));  // Le rouge diminue
        g = 255;
    }
    
    // Appliquer la couleur à divReussite
    divReussite.style.backgroundColor = `rgb(${r}, ${g}, ${b})`;
    
    // Calcul de la luminosité (0.299 * R + 0.587 * G + 0.114 * B)
    let luminosity = 0.299 * r + 0.587 * g + 0.114 * b;

    // Choisir noir ou blanc en fonction de la luminosité
    let textColor = (luminosity > 128) ? 'black' : 'white';

    // Appliquer la couleur du texte à divReussite
    divReussite.style.color = textColor;
    

    //Nombres
    let etiquette=document.createElement('span');
    etiquette.classList.add('etiquette-stat');
    etiquette.innerText=nbDebut;
    ligne.appendChild(etiquette);
    etiquette=document.createElement('span');
    etiquette.classList.add('etiquette-stat');
    etiquette.innerText=nbMobile;
    if (juste) {etiquette.style.backgroundColor='rgb(0, 255, 0)'}
    else {etiquette.style.backgroundColor='yellow';}
    ligne.appendChild(etiquette);
    if (!juste){
        etiquette=document.createElement('span');
        etiquette.classList.add('etiquette-stat');
        etiquette.style.backgroundColor='red';
        etiquette.innerText=reponse;
        ligne.appendChild(etiquette); 
    }
    etiquette=document.createElement('span');
    etiquette.classList.add('etiquette-stat');
    etiquette.innerText=nbFin;
    ligne.appendChild(etiquette);
    //Aides
    if (aidePremiereGraduation){
        etiquette=document.createElement('span');
        etiquette.classList.add('etiquette-stat','aide');
        etiquette.style.backgroundImage='url(images/graduation.svg)';
        ligne.appendChild(etiquette);
    } 
    if (aideGraduations){
        etiquette=document.createElement('span');
        etiquette.classList.add('etiquette-stat','aide');
        etiquette.style.backgroundImage='url(images/graduations.svg)';
        ligne.appendChild(etiquette);
    } 
    if (aideBlocUnite){
        etiquette=document.createElement('span');
        etiquette.classList.add('etiquette-stat','aide');
        etiquette.style.backgroundImage='url(images/bloc.svg)';
        ligne.appendChild(etiquette);
    } 

}

function effaceStats(){
    if (modeFonctionnement === 'eleve') {
        let contenuStats = JSON.stringify(resultats.innerHTML);
        let score = '';
        let numberOfQuestions = '';
        let contenuAEnvoyer = {score, numberOfQuestions, type: 'mathaleaSendScore', numeroExercice, contenuStats};
        // Renvoyer le résultat
        window.parent.postMessage(contenuAEnvoyer, '*');
    } else {
        resultats.innerHTML=spanTauxReussite.innerHTML='';
        reussites=[0,0];
    }
}



function exerciceSuivant(){
    console.log("ExerciceSuivant");
    const boutonsActifs = document.querySelectorAll('.actif');
    boutonsActifs.forEach(bouton=>{
        bouton.classList.remove('actif');
    });
    blocUnite.style.top=blocUnite.style.left=null;

    etiquetteMobile.innerText='?';
    nombre.innerText='?';
    zoneSaisie.style.color='gray';
    zoneSaisie.style.backgroundColor = null;
    bornes=tirerBornesAuSort(a,b);
    if (lockA){nbDebut=a}
    else{nbDebut=bornes[0];}
    if (lockB){nbFin=b}
    else{nbFin=bornes[1];}
    majEtiquettes(nbDebut,nbFin);
    afficheNombreMobile();
    creeGraduations();
    adapteBlocs();
    juste=false;
    suivant.classList.add('hide');
    suivantbis.classList.add('transparent');
    ok.classList.remove('hide');
    nombre.style.fontSize='64px';
    aidePremiereGraduation=false;
    aideGraduations=false;
    aideBlocUnite=false;
    erreur.classList.add('hide');
    etiquetteMobile.style.backgroundColor=null;
    boutonsReponses.forEach(bouton => {
        bouton.style.backgroundColor=null;
    });

}

function adapteBlocs(){
    const blocs = droite.querySelectorAll('.bloc-unite');
    blocs.forEach(bloc=>{
        bloc.style.width=ecartement+'%';
        bloc.classList.add('hide');
    });
}

function creeGraduations(){
    const graduations = droite.querySelectorAll('.toutes');
    graduations.forEach(graduation=>{
        graduation.remove();
    });


    let ecart = nbFin - nbDebut;
    let nombreDeGraduationsACreer;
    let multiple;

    if (ecart <= 100) {    
        nombreDeGraduationsACreer = ecart - 1;
        ecartement = 100 / ecart;
        multiple = 10;
        facteur = 1;
    } else if (ecart <= 1000) {
        nombreDeGraduationsACreer = Math.floor(ecart / 10);
        ecartement = 1000 / ecart;
        multiple = 100;
        facteur = 10;
    } else {
        nombreDeGraduationsACreer = Math.floor(ecart / 100);
        ecartement = 10000 / ecart;
        multiple = 1000;
        facteur = 100;
    }
    console.log("écart : "+ecart)
    console.log("nombre de graduations à créer : "+nombreDeGraduationsACreer)

    blocUnite.innerHTML = facteur;

    premiereGraduation.style.left= ecartement + '%';
    premiereGraduation.classList.add('transparent');


    for (let i = 0; i < nombreDeGraduationsACreer; i++) {
        let nouvelleGraduation = document.createElement('div');
        let nombrePosition = multiple/10 * (i + 1) + nbDebut;
        console.log("Nombre position "+nombrePosition);
        nouvelleGraduation.classList.add('graduation','toutes','transparent');
        if (nombrePosition % multiple === 0 ) {nouvelleGraduation.classList.add('multiple');}
        let decalage = nbDebut % (multiple/10);
        console.log("decalage : "+decalage);
        let decalagePc = (decalage / ecart) * 100;
        console.log("decalage % "+decalagePc);
        nouvelleGraduation.style.left= (i+1) * ecartement - decalagePc + '%';
        droite.appendChild(nouvelleGraduation);
    }
}

function visibilite(classe,mode){
    let classeCache;
    if (mode==='transparent'){classeCache=mode;}
    else {classeCache='hide';}
    const objets = document.querySelectorAll('.'+classe);
    objets.forEach(objet=>{
        objet.classList.toggle(classeCache);
    });
}

function estEnfantDe(element,parent) {
    // Parcours les parents de l'élément donné
    while (element.parentNode) {
        element = element.parentNode;
        // Si un des parents est l'élément menu, retourne vrai
        if (element === parent) {
            return true;
        }
    }
    // Si aucun parent n'est l'élément menu, retourne faux
    return false;
}

function clic(event){

    console.log('clic');
    let cible = event.target;

    if (cible!=menu && !estEnfantDe(cible,menu) && cible !=boutonMenu){visibiliteMenu('ferme');}
    if (cible!=menu && !estEnfantDe(cible,stats) && cible !=boutonStats){visibiliteStats('ferme');}

    
    if (cible.classList.contains('draggable')) {
        posX = event?.targetTouches?.[0]?.clientX || event.clientX;
        posY = event?.targetTouches?.[0]?.clientY || event.clientY;
        dragged = cible;
        event.preventDefault();
        posX_objet = dragged.offsetLeft;
        posY_objet = dragged.offsetTop;
        diffsourisx = (posX - posX_objet);
        diffsourisy = (posY - posY_objet);
        dragged.classList.add('dragged');
    }
}

function move(event) {
    event.preventDefault();  // Empêche le comportement par défaut (utile pour les événements tactiles)

    // Vérifie si un élément est en train d'être "dragged"
    if (dragged) {
        console.log('dragged');
        
        // Obtention des coordonnées en mode tactile ou souris
        const posX = event?.targetTouches?.[0]?.clientX || event.clientX;
        const posY = event?.targetTouches?.[0]?.clientY || event.clientY;

        console.log('posX ' + posX);

        // Calcul des nouvelles positions de l'élément en fonction de la différence entre la souris et l'élément
        dragged.style.left = (posX - diffsourisx) + "px";
        dragged.style.top = (posY - diffsourisy) + "px";
    }
}


function release(event) {
    if (dragged) {
        dragged.classList.remove('dragged');
        dragged=null;
    }

}

function info(){
    let texte_info='Estimation est une application libre sous licence GNU/GPL.\nPar Arnaud Champollion\nPolice : Écriture A Rom de https://eduscol.education.fr\nsous licence CC BY ND'
    alert(texte_info);
}

function visibiliteMenu(mode){
    if (mode==='ouvre' && menu.style.left==='-415px'){
        menu.style.left='0px';
        menuOn=true;
    }
    else {
        menu.style.left='-415px';
        ok.focus();
        menuOn=false;
    }
}

function verifieValeurA(){
    if (a>b-2){
        b=a+2;
        choixB.value=b;
    }
}

function verifieValeurB(){
    if (a>b-2){
        a=b-2;
        choixA.value=a;
    }
}

function visibiliteStats(mode){
    if (mode==='ouvre' && stats.style.right==='-415px'){stats.style.right='0px';}
    else {stats.style.right='-415px';ok.focus();}
}

// Tactile
document.addEventListener("touchstart", clic);
document.addEventListener("touchend", release);
document.addEventListener('touchmove', function(event) {
    event.preventDefault();
    move(event);
}, { passive: false });
// Souris
document.addEventListener("mousedown", clic);
document.addEventListener("mousemove", move);
document.addEventListener("mouseup", release);



// Écouteurs de clavier
document.addEventListener('keydown', function(event) {
    if (!menuOn){
        if (!isNaN(event.key)){
            entreNombre(event.key);
        }
        else if (event.key === "-"){
            entreNombre(event.key);
        } else if (event.key === "Backspace"){
            entreNombre('⌫');
        } else  if (event.key === "Enter") {
            verifieReponse();
        }
    }    
  });

  // Vérifie la hauteur de l'écran lorsque la page est chargée
window.onload = function() {
    checkScreenHeight();
  };
  
  // Vérifie la hauteur de l'écran lorsque la fenêtre est redimensionnée
  window.onresize = function() {
    checkScreenHeight();
  };
  
  
  // Fonction pour vérifier la hauteur de l'écran

  function checkScreenHeight() {
    var screenHeight = window.innerHeight;
    if (screenHeight < 420) {
        if (!petitEcran){
            zoneSaisie=etiquetteMobile;
            etiquetteMobile.innerText=nombre.innerText;
            etiquetteMobile.style.backgroundColor=window.getComputedStyle(nombre).getPropertyValue("background-color");
        }
        petitEcran=true;

      // Ajoutez ici le code que vous souhaitez exécuter si la hauteur de l'écran est inférieure à 420 pixels
    } else {        
        zoneSaisie=nombre;
        if (petitEcran){
            nombre.innerText=etiquetteMobile.innerText;
            etiquetteMobile.innerText='?';
            console.log(window.getComputedStyle(etiquetteMobile).getPropertyValue("background-color"))
            nombre.style.backgroundColor=window.getComputedStyle(etiquetteMobile).getPropertyValue("background-color");
        }
        petitEcran=false;
    }
  }


function ouvre(div){
    console.log('ouverture fenêtre')
    div.classList.remove('hide');
    darkbox.classList.remove('hide');
}

function ferme(div){
    console.log('fermeture fenêtre')
    div.classList.add('hide');
    darkbox.classList.add('hide');
}

// Lancement du premier exercice
exerciceSuivant();